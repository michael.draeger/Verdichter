import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import Test_1

# Verdampfertemperatur Auswählen
start_temp_eva = -15
end_temp_eva = 15
step_eva = 5
temperatures_eva = list(range(start_temp_eva, end_temp_eva + 1, step_eva))

# Kondensatortemperatur Auswählen
start_temp_cond = 30
end_temp_cond = 70
step_cond = 5
temperatures_cond = list(range(start_temp_cond, end_temp_cond + 1, step_cond))


# Auswählen des Verdichters
table_data = np.array(Test_1.generate_table(temperatures_eva, temperatures_cond))


# Größe der Tabelle
rows, cols = table_data.shape

# Koordinaten erstellen mit angepasster Schrittweite
y = np.arange(start_temp_eva, end_temp_eva + 1 , step_eva)
x = np.arange(start_temp_cond, end_temp_cond + 1, step_cond)
X, Y = np.meshgrid(x, y)

# Z-Koordinaten (Höhenwerte) aus der Tabelle extrahieren
Z = table_data

# 3D-Plot erstellen
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X.ravel(), Y.ravel(), Z.ravel())
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()

# Tabelle plotten mit angepassten Parametern
surf = ax.plot_surface(X, Y, Z, cmap='viridis', edgecolor='none')

# Achsenbeschriftungen
ax.set_xlabel('Verdampfertemperatur')
ax.set_ylabel('Kondensatortemperatur')
ax.set_zlabel('Isentroper Verdichterwirkungsgrad')

# Achsenlimits festlegen
ax.set_xlim(start_temp_eva, end_temp_eva)
ax.set_ylim(start_temp_cond, end_temp_cond)
ax.set_zlim(np.min(Z), np.max(Z))

# Farblegende hinzufügen
fig.colorbar(surf, shrink=0.5, aspect=5)

# Plot anzeigen
plt.show()


Test_1.print_table(table_data, temperatures_eva, temperatures_cond)