from rp_wrapper import RefProp

temperatures_eva = []
temperatures_cond = []

def calculate_value(temperature_eva, temperature_cond):

    # Fluid
    curr_fluid = "R134a"
    # Create instance of RefProp
    refProp = RefProp(curr_fluid)

    n = 2900/60  # Umdrehungen pro Sekunde
    Hubvolumen = 437.5*1e-6  # Hubvolumen in m³

    t_eva = temperature_eva
    t_con = temperature_cond
    T_eva = t_eva + 273.15
    T_con = t_con + 273.15
    t_superheat = 10

    T_1 = T_eva + t_superheat

    state_eva_out = refProp.calc_state("TQ", T_eva, 1)
    state_1 = refProp.calc_state("PT", state_eva_out.p, T_1)
    state_3 = refProp.calc_state("TQ", T_con, 0)
    state_2_isentropic = refProp.calc_state("PS", state_3.p, state_1.s)
    state_4 = refProp.calc_state("PH", state_1.p, state_3.h)



    a_0 = 30.74420166 + t_superheat  # 30,74426166 + Überhitzung
    a_1 = 0.403667986
    a_2 = 0.272141993
    a_3 = 0.034843002
    a_4 = - 0.030188199
    a_5 = 0.0114189
    a_6 = 0
    a_7 = 0
    a_8 = 0
    a_9 = 0

    t_2 = a_0 + \
            a_1 * t_eva + \
            a_2 * t_con + \
            a_3 * t_eva ** 2 + \
            a_4 * t_eva * t_con + \
            a_5 * t_con ** 2 + \
            a_6 * t_eva ** 3 + \
            a_7 * t_con * t_eva ** 2 + \
            a_8 * t_eva * t_con ** 2 + \
            a_9 * t_con ** 3 + 273.15

    state_2 = refProp.calc_state("PT", state_3.p, t_2)
    pi = state_2.p / state_1.p

    eta_isentropic = (state_2_isentropic.h - state_1.h) / (state_2.h - state_1.h)


    # Kälteleistung:
    a_0 = 56258.39844
    a_1 = 2250.969971
    a_2 = - 301.4349976
    a_3 = 36.59339905
    a_4 = - 10.71210003
    a_5 = - 2.58088994
    a_6 = 0.209046006
    a_7 = - 0.211298004
    a_8 = - 0.066189103
    a_9 = 0.0102589

    Q =   a_0 + \
          a_1 * t_eva + \
          a_2 * t_con + \
          a_3 * t_eva ** 2 + \
          a_4 * t_eva * t_con + \
          a_5 * t_con ** 2 + \
          a_6 * t_eva ** 3 + \
          a_7 * t_con * t_eva ** 2 + \
          a_8 * t_eva * t_con ** 2 + \
          a_9 * t_con ** 3 + 273.15

    # Motorleistung:
    a_0 = 2756.719971
    a_1 = - 19.27099991
    a_2 = 272.5499878
    a_3 = - 0.450515002
    a_4 = 0.992106974
    a_5 = - 2.543770075
    a_6 = 0.016642099
    a_7 = 0.00913956
    a_8 = 0.00389873
    a_9 = 0.033645201

    P = a_0 + \
          a_1 * t_eva + \
          a_2 * t_con + \
          a_3 * t_eva ** 2 + \
          a_4 * t_eva * t_con + \
          a_5 * t_con ** 2 + \
          a_6 * t_eva ** 3 + \
          a_7 * t_con * t_eva ** 2 + \
          a_8 * t_eva * t_con ** 2 + \
          a_9 * t_con ** 3 + 273.15

    COP_Cool = Q/P
    COP_Heat =(Q + P) / P
    x = Q + P
    massflow_ref = (Q + P) / (state_2.h - state_3.h)
    Dichte_1 = state_1.d  # Dichte in kg/m^3
    Volumenstrom_1 = massflow_ref / state_1.d
    Volumenstrom_Theoretisch = n * Hubvolumen

    eta_vol = Volumenstrom_1/Volumenstrom_Theoretisch

    #eta_isen über Massflow (über Q) und P berechnen
    h2_Test = (P / massflow_ref) + state_1.h
    state_2_Test = refProp.calc_state("PH", state_2.p, h2_Test)
    T_2_Test = state_2_Test.T
    eta_isen_Test = (state_2_isentropic.h - state_1.h) / (state_2_Test.h - state_1.h)

    #print(COP_Heat)
    #print(pi)
    #print(eta_vol)
    #print(eta_isentropic)
    #print (t_2)
    #print (state_1.p)
    #print (state_1.T)
    #print (p_2)
    #print(state_1.p)
    #print (Te+273.15)
    #print (Tc+273.15)
    #print (massflow_cond)

    value = round(eta_isentropic, 4)
    #value = round(state_2.p/state_1.p, 4)

    return value

def generate_table(temperatures_eva, temperatures_cond):
    table = []

    for temperature_eva in temperatures_eva:
        row = []
        for temperature_cond in temperatures_cond:
            value = calculate_value(temperature_eva, temperature_cond)
            row.append(value)
        table.append(row)

    return table

def print_table(table, temperatures_eva, temperatures_cond):
    headers = [''] + [str(temp) for temp in temperatures_cond]
    header_row = "\t".join("{:<6}".format(header) for header in headers)
    print(header_row)

    for i in range(len(table)):
        row = [str(temperatures_eva[i])] + [str(value).replace('.', ',') for value in table[i]]
        row_str = "\t".join("{:<6}".format(cell) for cell in row)
        print(row_str)
'''
# Verdampfertemperatur Auswählen
start_temp_eva = -15
end_temp_eva = 15
step_eva = 5
temperatures_eva = list(range(start_temp_eva, end_temp_eva + 1, step_eva))

# Kondensatortemperatur Auswählen
start_temp_cond = 30
end_temp_cond = 70
step_cond = 5
temperatures_cond = list(range(start_temp_cond, end_temp_cond + 1, step_cond))

table = generate_table(temperatures_eva, temperatures_cond)
print_table(table, temperatures_eva, temperatures_cond)
'''