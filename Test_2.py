import numpy as np
import matplotlib.pyplot as plt

# Beispiel-Daten
x = np.array([1, 2, 3, 4, 5])
y = np.array([2, 4, 6, 8, 10])
z = np.array([0.1, 0.4, 0.6, 0.8, 1.0])

# Tabelle erstellen
table = np.column_stack((x, y, z))


# 3D-Plot erstellen
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()